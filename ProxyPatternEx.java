package bin.proxy.demo;

import bin.proxyClasses.*;

public class ProxyPatternEx{


	public static void main (String args[]){

		System.out.println("Proxy demo");
		Proxy proxy = new Proxy();
		proxy.doSomeWork();
	}

}